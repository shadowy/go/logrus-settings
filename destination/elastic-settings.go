package destination

import (
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/writer"
	"os"
)

// ElasticSettings - settings for elastic.
type ElasticSettings struct {
	URL               string  `yaml:"url"`
	Host              string  `yaml:"host"`
	User              string  `yaml:"user"`
	Password          string  `yaml:"password"`
	Level             *string `yaml:"level"`
	FlushInterval     *int64  `yaml:"pushTime"`
	IndexName         *string `yaml:"indexName"`
	IndexSuffixFormat *string `yaml:"indexSuffixFormat"`
}

// Init - init settings.
func (settings *ElasticSettings) Init() {
	settings.Level = isNullString(settings.Level, "info")
	settings.FlushInterval = isNullInt64(settings.FlushInterval, 10)
	settings.IndexName = isNullString(settings.IndexName, "logs")
	settings.IndexSuffixFormat = isNullString(settings.IndexSuffixFormat, "2006-01-02")

	var cfg []elastic.ClientOptionFunc
	cfg = append(cfg, elastic.SetURL(settings.URL))

	if settings.User != "" || settings.Password != "" {
		logrus.Info("ElasticSettings.Init use basicAuth")

		cfg = append(cfg, elastic.SetBasicAuth(settings.User, settings.Password))
	}

	_, err := elastic.NewSimpleClient(cfg...)
	if err != nil {
		logrus.WithFields(logrus.Fields{"url": settings.URL}).WithError(err).Error("ElasticSettings.Init NewClient")
		return
	}

	logrus.AddHook(&writer.Hook{ // Send logs with level higher than warning to stderr
		Writer: os.Stderr,
		LogLevels: []logrus.Level{
			logrus.PanicLevel,
			logrus.FatalLevel,
			logrus.ErrorLevel,
			logrus.WarnLevel,
			logrus.DebugLevel,
		},
	})
	/*hook, err := logrus.AddHookNewElasticHook(client, settings.Host, utils.StringToLogLevel(*settings.Level),
	func() string {
		return fmt.Sprintf("%s-%s", *settings.IndexName, time.Now().Format(*settings.IndexSuffixFormat))
	}, time.Millisecond*time.Duration(*settings.FlushInterval))*/
	if err != nil {
		logrus.WithFields(logrus.Fields{"url": settings.URL}).WithError(err).Error("ElasticSettings.Init NewElasticHook")
		return
	}

	//logrus.AddHook(hook)
}
