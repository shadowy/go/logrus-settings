// +build windows

package destination

import (
	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
)

// InitConsole - init console for logrus
func InitConsole() {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	logrus.SetOutput(colorable.NewColorableStdout())
}
