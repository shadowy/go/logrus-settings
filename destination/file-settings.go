package destination

import (
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/logrus-settings/utils"
	"path/filepath"
	"time"
)

const hoursInDay = 24

// FileSettings - settings for file.
type FileSettings struct {
	Filename  string `yaml:"filename"`
	DaysStore int    `yaml:"daysStore"`
	Level     string `yaml:"level"`
}

// Init - init settings.
func (settings *FileSettings) Init() {
	if settings.Level == "" {
		settings.Level = "info"
	}

	var level = utils.StringToLogLevel(settings.Level)

	logrus.SetLevel(level)

	var err error
	settings.Filename, err = filepath.Abs(settings.Filename)

	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"filename":  settings.Filename,
			"level":     settings.Level,
			"daysStore": settings.DaysStore,
		}).Error("FileSettings.Init abs")
	}

	writer, _ := rotatelogs.New(
		settings.Filename+".%Y%m%d", rotatelogs.WithLinkName(settings.Filename),
		rotatelogs.WithMaxAge(time.Duration(hoursInDay*settings.DaysStore)*time.Hour),
		rotatelogs.WithRotationTime(time.Duration(hoursInDay)*time.Hour))
	pathMap := lfshook.WriterMap{}

	for _, l := range []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel, logrus.WarnLevel,
		logrus.InfoLevel, logrus.DebugLevel} {
		if l <= level {
			pathMap[l] = writer
		}
	}

	logrus.AddHook(lfshook.NewHook(pathMap, &logrus.TextFormatter{}))
}
