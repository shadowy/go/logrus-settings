package destination

func isNullString(value *string, def string) *string {
	if value != nil {
		return value
	}

	return &def
}

func isNullInt64(value *int64, def int64) *int64 {
	if value != nil {
		return value
	}

	return &def
}
