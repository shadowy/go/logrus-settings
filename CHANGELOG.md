# CHANGELOG

<!--- next entry here -->

## 1.1.2
2023-09-18

### Fixes

- disable elastic search (321706ca3229e0088c632ef7b1bd6f0aa42a5895)
- disable elastic search (90fc61a9bc6da194aae39e0b2b4453232a60ca60)
- disable elastic search (f8ad079f5b9f35c43e42d56b4778db281424ca10)

## 1.1.1
2020-05-02

### Fixes

- fix issue with null value (1cd3404b449ed1654d8edce129ff7affd63d3343)

## 1.1.0
2020-04-29

### Features

- add user and password for elastic search (36aae38770f18a4cf0434084f9db2fb2cee1054d)

### Fixes

- update library and fix lint issues (59344a76ecfa3789651510bfc6d88bff83e9dcee)
- fix CHANGELOG.md (968ee9a9ea1bc45d49738a5f32186a44d38c852e)
- update CHANGELOG.md (9882c27c371b9a417e6201961e0ce1daff28faef)
- reset changes (3bb2e4d9c03dbf5cb793dbac14ce063b832012de)

