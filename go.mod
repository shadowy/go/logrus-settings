module gitlab.com/shadowy/go/logrus-settings

go 1.12

require (
	github.com/lestrrat-go/file-rotatelogs v2.3.0+incompatible
	github.com/lestrrat-go/strftime v0.0.0-20180821113735-8b31f9c59b0f // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/olivere/elastic/v7 v7.0.4
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
)
