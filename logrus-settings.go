package logsettings

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/logrus-settings/destination"
	"io/ioutil"
)

// Settings - settings for logging.
type Settings struct {
	File         *destination.FileSettings    `yaml:"file"`
	Elastic      *destination.ElasticSettings `yaml:"elastic"`
	BlockConsole *bool                        `yaml:"blockConsole"`
}

// Init - init settings.
func (settings *Settings) Init() error {
	logrus.SetLevel(logrus.DebugLevel)

	if settings.BlockConsole == nil {
		settings.BlockConsole = getPtrBool(false)
	}

	if settings.File != nil {
		settings.File.Init()
	}

	if settings.Elastic != nil {
		settings.Elastic.Init()
	}

	if *settings.BlockConsole {
		logrus.SetOutput(ioutil.Discard)
	} else {
		destination.InitConsole()
	}

	return nil
}

func getPtrBool(value bool) *bool {
	return &value
}
